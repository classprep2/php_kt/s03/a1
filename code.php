<?php



class Person {
	
	public $firstName;
	public $middleName;
	public $lastName;

	
	public function __construct($firstName, $middleName, $lastName){
		$this->firstName=$firstName;
		$this->middleName = $middleName;
		$this->lastName=$lastName;
		$this->fullName=$firstName." ".$lastName;
	}

	public function printName(){
		return "The fullname is $this->fullName";
	}
}

$person = new Person('Senku', 'K.', 'Ishigami');


class Developer extends Person{

	public function printName(){
		return "The name is $this->fullName and you are a developer";
	}

	/* ENCAPSULATION */
	// getters(read-only) && setters(write-only)
	// Encapsulating properties
	/* Also helps us to make data to be hidden*/
	public function getName(){
		return $this->fullName;
	}

	public function setName($fullName){
		$this->fullName=$fullName;
	}
}

$developer = new Developer('John Finch', 'F.', 'Smith');

// ------------------------------

class Engineer extends Person{

	public function printName(){
		return "You are an engineer named $this->fullName";
	}

	public function getName(){
		return $this->fullName;
	}

	public function setName($fullName){
		$this->fullName=$fullName;
	}
}

$engineer = new Engineer('Harold Myers', 'F.', 'Reese');
?>

